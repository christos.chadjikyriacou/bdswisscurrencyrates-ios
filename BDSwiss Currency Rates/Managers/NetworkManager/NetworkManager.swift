//
//  NetworkManager.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 06/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit
import Combine


typealias NetworkResponseCompletionHandler<ResponseObject:Decodable> = (Result<NetworkManager.Response<ResponseObject>,Error>)->Void

class NetworkManager:NSObject {
    
    
    static let shared = NetworkManager()
    
    struct Request {
        let url:URL
        let httpMethod:HTTPMethod
        
        var urlRequest:URLRequest  {
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = httpMethod.rawValue
            return urlRequest
        }
    }
    
    struct Response<ResponseObject:Decodable> {
        let data:ResponseObject
        let response: URLResponse?
    }
    
    private lazy var session = URLSession(configuration: .default, delegate: SecurityManager.shared, delegateQueue: nil)
    
    
    func run<ResponseObject: Decodable>(_ request:Request) throws -> AnyPublisher<ResponseObject,Error> {
        return session
            .dataTaskPublisher(for:  request.urlRequest)
            .tryMap() { element -> Data in
                guard let statusCode = (element.response as? HTTPURLResponse)?.statusCode, 200..<300 ~= statusCode else {
                    throw URLError(.badServerResponse)
                }
                
                guard !element.data.isEmpty else {
                    throw URLError(.zeroByteResource)
                }
                
                return element.data
            }
            .decode(type: ResponseObject.self, decoder: JSONDecoder())
            .receive(on:DispatchQueue.main)
            .eraseToAnyPublisher()
        
    }
    
}



extension NetworkManager.Response where ResponseObject:ExpressibleByNilLiteral {
    
    init(response: URLResponse) {
        self.init(data:nil, response:response)
    }
}

extension NetworkManager.Request {
    
    init(urlString:String,httpMethod:HTTPMethod) throws {
        
        let url = try urlString.toURLOrThrow()
        
        self.init(url: url, httpMethod: httpMethod)
    }
}





