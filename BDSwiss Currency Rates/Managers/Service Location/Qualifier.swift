//
//  ServiceKey.swift
//  BDSwiss Currency
//
//  Created by Christos Chadjikyriacou on 21/03/2020.
//  Copyright © 2020 NetInfo plc. All rights reserved.
//

import Foundation


struct Qualifier {
    internal let serviceType: Any.Type
    internal let name: String?
}

extension Qualifier: Hashable {
    public func hash(into hasher: inout Hasher) {
        String(describing:serviceType).hash(into: &hasher)
        name?.hash(into: &hasher)
    }
}


func == (lhs: Qualifier, rhs: Qualifier) -> Bool {
    return lhs.serviceType == rhs.serviceType && lhs.name == rhs.name
}

