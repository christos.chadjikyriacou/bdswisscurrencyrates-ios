//
//  DependenciesContainer.swift
//  BDSwiss Currency
//
//  Created by Christos Chadjikyriacou on 21/03/2020.
//  Copyright © 2020 NetInfo plc. All rights reserved.
//

import Foundation



open class DependeciesContainer:HasDefaultIplementation {
    
    public static var shared = DependeciesContainer()
    
    
    enum RegistraionType {
        case single
        case factory
    }
    
    private var registeredServices:[Qualifier:Any] = [:]
    
    
    /// Registers a service to the containrt
    /// - Parameters:
    ///   - registrationType: Sigleton or lazy
    ///   - serviceType: The type of the service
    ///   - name: A string identifer
    ///   - resolve: A method the container uses to initialize your service
    func register<Service>(registrationType:RegistraionType = .factory,
                           serviceType:Service.Type,name:String? = nil,
                           resolve:@escaping ()->Service) {
        let qualifier = Qualifier(serviceType: serviceType, name: name)
        
        switch registrationType {
        case .factory:
            registeredServices[qualifier] = resolve
        case .single:
            registeredServices[qualifier] = resolve()
        }
        
        
    }
    
    
    /// Call this method when you want the registed iplementation of a service
    /// - Parameters:
    ///   - serviceType: The type of the service you want to get
    ///   - name: A string identifer
    /// - Returns: The service object
    public func resolve<Service>(serviceType:Service.Type,name:String? = nil) -> Service? {
        let qualifier = Qualifier(serviceType: serviceType, name: name)
        let foundService = registeredServices[qualifier]
        if let registeredSigletonService = foundService as? Service {
            return registeredSigletonService
        }
        else if let registeredLazyService = foundService as? ()->Service {
            return registeredLazyService()
        }
        
        return nil
    }
    
    
    
}


