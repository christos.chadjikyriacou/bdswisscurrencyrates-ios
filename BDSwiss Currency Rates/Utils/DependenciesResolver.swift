//
//  DependencyManager + Starter.swift
//  VTB Armenia
//
//  Created by Christos Chadjikyriacou on 21/03/2020.
//  Copyright © 2020 NetInfo plc. All rights reserved.
//          

import Foundation
import UIKit.UIApplication

class DependenciesResolver:Starter {
    
    
    @Default
    private static var container:DependeciesContainer
    
    
    public static func start() {
        registerSingletons()
        registerRepositories()
        registerViewModels()
    }
    
    private static func registerSingletons() {
        
        container.register(
            registrationType: .single,
            serviceType: PropertiesManager.self,
            resolve: {PropertiesManagerImpl()})
        
        
        container.register(
            registrationType: .single,
            serviceType: TargetManager.self,
            resolve: {TargetManager.shared})
        
        container.register(
            registrationType:.single,
            serviceType: NetworkManager.self,
            resolve: {NetworkManager.shared})
        
        
        container.register(
            registrationType:.single,
            serviceType: UIApplication.self,
            resolve: {UIApplication.shared})
        
        container.register(
            registrationType:.single,
            serviceType: NotificationCenter.self,
            resolve: {NotificationCenter.default})
    }
    
    private static func registerRepositories() {
        container.register(
            registrationType: .single,
            serviceType: RatesRepository.self,
            resolve: {RatesRepositoryImpl()})
    }
    
    
    private  static func registerViewModels() {
        container.register(
            registrationType: .factory,
            serviceType: RatesViewModel.self,
            resolve: {RatesViewModel()})
        
    }
    
    
}




