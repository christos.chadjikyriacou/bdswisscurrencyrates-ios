//
//  XibView.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 08/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit


open class XibView:UIView {
    
    var contentView: UIView! {
        willSet {
            
            contentView?.removeFromSuperview()
            newValue.frame = bounds
            newValue.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            newValue.backgroundColor = .clear
            addSubview(newValue)
            
        }
    }
    

    public init() {
        super.init(frame:.zero)
        let objectName = String(describing: type(of:  self))
        initialization(nibName: objectName)
        initialization()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        let objectName = String(describing: type(of:  self))
        initialization(nibName: objectName)
        initialization()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let objectName = String(describing: type(of:  self))
        initialization(nibName: objectName)
        initialization()
    }
    
    
    open func initialization() {
        
    }
   
    
    open func prefferedBundle() -> Bundle? {
        return Bundle.init(for: XibView.self)
    }
    
    open func initialization(nibName:String) {
        let nib = UINib(nibName: nibName, bundle: prefferedBundle())
        let view = (nib.instantiate(withOwner: self, options: nil).first as? UIView) ?? UIView()
        contentView = view
    }
    
}
