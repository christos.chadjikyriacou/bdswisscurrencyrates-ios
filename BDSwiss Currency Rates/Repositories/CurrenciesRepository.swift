//
//  CurrenciesRepository.swift
//  BDSwiss Currency Rates
//
//  Created by Christos on 10/5/20.
//

import Foundation


protocol RateasRepository {
    func get(onCompletion:@escaping NetworkResponseCompletionHandler<RatesObject>)
}

class RatesRepositoryImpl:RateasRepository {
    
    
    @Inject
    private var client:NetworkManager
    
    
    func get(onCompletion:@escaping NetworkResponseCompletionHandler<RatesObject>)  {
        let endpoint =  Endpoints.rates
        do {
            let request = try NetworkManager.Request(urlString: endpoint, httpMethod: .get)
            client.run(request, onCompletion: onCompletion)
        }
        catch(let error) {
            onCompletion(.failure(error))
        }
        
    }
    
}
