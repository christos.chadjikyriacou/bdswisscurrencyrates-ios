//
//  CurrenciesRepository.swift
//  BDSwiss Currency Rates
//
//  Created by Christos on 10/5/20.
//

import Foundation
import Combine

protocol RatesRepository {
    func getRates() throws -> AnyPublisher<RatesObject,Error>
    
}

class RatesRepositoryImpl:RatesRepository {
    
    
    @Inject private var client:NetworkManager
    
    
    func getRates() throws -> AnyPublisher<RatesObject,Error> {
        let endpoint =  Endpoints.rates
        let request =  try NetworkManager.Request(urlString: endpoint, httpMethod: .get)
        return try client.run(request)
        
    }
    
}
