//
//  Double + Extensions.swift
//  BDSwiss Currency Rates
//
//  Created by Christos on 10/5/20.
//

import Foundation

extension Double {
    func toString(with digits:Int) -> String {
        guard digits >= 0 else { return "" }
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = digits
        formatter.maximumFractionDigits = digits
        return formatter.string(from: NSNumber(value: self)) ?? "\(self)"
    }
}
