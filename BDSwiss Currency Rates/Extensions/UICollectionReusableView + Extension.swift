//
//  UICollectionReusableView + Extension.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 11/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit

extension UICollectionReusableView {
    
    static func
        create(collectionView:UICollectionView,kind:String,indexPath:IndexPath,reuseIdentifier:String? = nil) -> Self {
        
        return create(collectionView: collectionView,kind:kind,indexPath:indexPath, reuseIdentifier: (reuseIdentifier ??  String(describing:self)), type: self)
    }
    
    
    fileprivate static func create<T : UICollectionReusableView>(collectionView:UICollectionView,kind:String,indexPath:IndexPath,reuseIdentifier:String,type: T.Type) -> T {
    
        let nib = UINib(nibName: reuseIdentifier, bundle: nil)
        collectionView.register(nib, forSupplementaryViewOfKind:kind , withReuseIdentifier: reuseIdentifier)
        guard let cell = collectionView.dequeueReusableSupplementaryView(ofKind:kind,withReuseIdentifier: reuseIdentifier, for: indexPath) as? T else {
            return create(collectionView: collectionView, kind: kind,indexPath:indexPath,reuseIdentifier:reuseIdentifier, type: type)
        }
        
        return cell
        
    }
    
}
