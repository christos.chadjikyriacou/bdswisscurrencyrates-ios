//
//  AnyCancellable + Extensions.swift
//  BDSwiss Currency Rates
//
//  Created by Christos on 10/5/20.
//

import Foundation
import Combine

extension Set where Element:Cancellable {
    
    func cancelAll() {
        self.forEach { (cancellable) in
            cancellable.cancel()
        }
    }
}
