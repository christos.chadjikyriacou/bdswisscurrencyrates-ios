//
//  UIActivityIndicatorView + Extensions.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 08/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit


extension UIActivityIndicatorView {
    
    var isLoading:Bool {
        get {
            return isAnimating
        }
        
        set {
            newValue ? startAnimating() : stopAnimating()
        }
    }
}
