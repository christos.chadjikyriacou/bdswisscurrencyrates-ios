//
//  String + Extensions.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 07/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation


extension String {
    
    enum DataError:String,Error {
        case invalidURL = "Invalid URL"
        case invalidDateFormat = "Invalid Date Format"
        case invalidBoolFormat = "Invalid Bool Format"
    }
    
    var toURL:URL? {
        return URL(string: self)
    }
    
    func toURLOrThrow() throws  ->  URL  {
        
        guard let url = self.toURL else {
            throw DataError.invalidURL
        }
        
        return url
    }
    
    
    var toDouble:Double? {
         Double(self)
    }
    
    func toDate(withFormat format:String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter.date(from: self)
    }
    
    func toDateOrThrow(withFormat format:String) throws -> Date {
        guard let date = self.toDate(withFormat: format) else {
            throw DataError.invalidDateFormat
        }
        
        return date
    }
    
}
