//
//  UIView + Extensions.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 09/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit

extension UIView {
    func addSubviewAndFillParent(view:UIView,padding:CGFloat = 0) {
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: padding),
            trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: padding),
            topAnchor.constraint(equalTo: view.topAnchor,constant: padding),
            bottomAnchor.constraint(equalTo: view.bottomAnchor,constant: padding)
        ])
    }

}

