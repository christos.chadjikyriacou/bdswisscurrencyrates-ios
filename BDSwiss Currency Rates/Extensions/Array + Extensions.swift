//
//  Array + Extensions.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 09/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation

extension Collection {
    subscript(safe index: Index) -> Element? {
        return startIndex <= index && index < endIndex ? self[index] : nil
    }
    
}
