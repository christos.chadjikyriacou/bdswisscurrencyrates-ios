//
//  UICollectionViewCell + Extensions.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 11/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    
    
    var collectionView:UICollectionView? {
        return superview as? UICollectionView
    }
    
    var indexInSection:Int? {
        guard let indexPath = collectionView?.indexPathForItem(at: center) else {return nil}
        return indexPath.row
    }
    
    var isFirst:Bool {
        guard let indexPath = collectionView?.indexPathForItem(at: center) else {return false}
        return indexPath.row == 0
        
    }
    
    var isLastInSection:Bool {
        guard let indexPath = collectionView?.indexPathForItem(at: center) else {
            return false
            
        }
        guard let totalRows = collectionView?.numberOfItems(inSection: indexPath.section) else {
            return false
            
        }
        
        return indexPath.row == totalRows - 1
    }
    
    var isLast:Bool {
        guard let indexPath = collectionView?.indexPathForItem(at: center) else {return false}
        guard let totalSections = collectionView?.numberOfSections else { return false}
        guard let totalRows = collectionView?.numberOfItems(inSection: indexPath.section) else {return false}
        return indexPath.section == totalSections - 1 &&  indexPath.row == totalRows - 1
    }
    
    static func create(collectionView:UICollectionView,indexPath:IndexPath,reuseIdentifier:String? = nil) -> Self {
        
        return create(collectionView: collectionView,indexPath:indexPath, reuseIdentifier: (reuseIdentifier ??  String(describing:self)), type: self)
    }
    
    fileprivate static func create<T : UICollectionReusableView>(collectionView:UICollectionView,indexPath:IndexPath,reuseIdentifier:String,type: T.Type) -> T {
        
        let nib = UINib(nibName: reuseIdentifier, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? T else {
            return create(collectionView: collectionView,indexPath:indexPath,reuseIdentifier:reuseIdentifier, type: type)
        }
        
        return cell
        
    }
    
    
}
