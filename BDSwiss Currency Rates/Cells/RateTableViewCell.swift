//
//  RateTableViewCell.swift
//  BDSwiss Currency Rates
//
//  Created by Christos on 10/6/20.
//

import UIKit

class RateTableViewCell: UITableViewCell {

    @Inject private var properties:PropertiesManager
    
    @IBOutlet weak var priceLabelContainer: UILabel!
    @IBOutlet weak var symbolLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel! 

    var rate:RateViewModel? {
        didSet {
            guard let rate = rate else { return }
            symbolLabel.text = rate.symbol
            priceLabel.text = rate.priceString
            setPriceChangeColor(for:rate)
        }
    
    }
    
    
    private func setPriceChangeColor(for rate:RateViewModel) {
        let changeColor = CATransition().apply(closure: {$0.duration = properties.priceChangeAnimationDuration})
        
        CATransaction.begin()

        CATransaction.setCompletionBlock {
            self.priceLabelContainer.layer.add(changeColor, forKey: nil)
            self.priceLabelContainer.backgroundColor = {
                guard let grouthFlow = rate.priceChange else {
                    return Asset.Colors.gray.color
                    
                }
                switch grouthFlow {
                case .equealOrHigher:
                    return Asset.Colors.green.color
                case .lower:
                    return Asset.Colors.red.color
                }
            }()
        }

        CATransaction.commit()

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        symbolLabel.text = nil
        priceLabel.text = nil
    }

    
    override func layoutSubviews() {
        super.layoutSubviews()
        priceLabelContainer.layer.cornerRadius = priceLabelContainer.frame.height / 2
    }
    
    static func create(tableView:UITableView,rate:RateViewModel) -> RateTableViewCell {
        RateTableViewCell.create(tableView: tableView).apply(closure: {$0.rate = rate})
    }
    
}
