//
//  MainNavigationController.swift
//  BDSwiss Currency Rates
//
//  Created by Christos on 10/5/20.
//

import Foundation

class MainNavigationController:BaseNavigationController {
    
    
    
    init() {
        super.init(nibName: nil, bundle: nil)
        setViewControllers([RatesViewController()], animated: false)
    }
    
    
   
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
