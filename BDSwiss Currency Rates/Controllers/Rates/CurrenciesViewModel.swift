//
//  CurrenciesViewModel.swift
//  BDSwiss Currency Rates
//
//  Created by Christos on 10/5/20.
//

import Foundation


protocol RatesViewModel {
    func getRates()
    var error:Error? { get }
    var isLoading:Bool { get }
    var rates:RatesObject { get }
}


class RatesViewModelImpl:RatesViewModel,ObservableObject {
    
    
    @Inject private var currenciesRepository:CurrenciesRepository
    
    @Published var error:Error?
    @Published var isLoading = false
    @Published var rates = RatesObject(rates: [])
    
    
    func getRates() {
        isLoading = true
        currenciesRepository.get { (response) in
            self.isLoading = false
            switch response {
            case .success(let object):
                self.rates = object.data
            case .failure(let error):
                self.error = error
            }
        }
    }
    
    
}
