//
//  RateViewModel.swift
//  BDSwiss Currency Rates
//
//  Created by Christos on 10/5/20.
//

import Foundation

struct RateViewModel {

    @Inject private var properties:PropertiesManager
    
    enum PriceChange {
        case equealOrHigher
        case lower
    }
    
    let priceChange:PriceChange?
    let symbol:String
    var  priceString:String {
        return price.toString(with: properties.priceFloatingPointPrecision)
    }
    let price:Double

    init(rate:Rate,previousPrice:Double? = nil) {
        self.symbol = rate.symbol
        self.price = rate.price

        self.priceChange = {
            guard let previousPrice = previousPrice else {  return nil }
            return previousPrice <= rate.price ? .equealOrHigher : .lower
        }()
    }

    
}



extension RateViewModel:Hashable {
    
    static func == (lhs: RateViewModel, rhs: RateViewModel) -> Bool {
        return lhs.symbol == rhs.symbol
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(symbol)
    }
    
}
