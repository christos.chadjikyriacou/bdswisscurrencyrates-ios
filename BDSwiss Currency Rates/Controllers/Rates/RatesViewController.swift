//
//  CurrenciesViewController.swift
//  BDSwiss Currency Rates
//
//  Created by Christos on 10/5/20.
//

import UIKit

class RatesViewController: BaseViewController {

    private enum Section {
        case main
    }

    @Inject private var viewModel:RatesViewModel
    
    private lazy var activityIndicatorView = UIActivityIndicatorView(style: .medium).apply(closure: {$0.hidesWhenStopped = true})
    private lazy var tableViewDataSource = UITableViewDiffableDataSource<Section,RateViewModel>(tableView: tableView, cellProvider: rateCellProvider)

    @IBOutlet weak private var tableView: UITableView! {
        didSet {
            tableView.dataSource = tableViewDataSource
            tableView.tableFooterView = UIView()
            tableView.allowsSelection = false
        }
    }

    private var error:Error? {
        didSet {
            guard let error = error else {
                hideError()
                return
            }
            showError(error: error, onDisappeared: viewModel.startGettingRates)
        }
    }
    private(set) var rates = [RateViewModel]() {
        didSet {
            updateRates(rates: rates)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.startGettingRates()
        configureNavigationBar()
    }

    override func configureObservers() {
        viewModel.$isLoadingPublisher.assign(to: \.isLoading, on: activityIndicatorView).store(in: &subscriptions)
        viewModel.$rates.assign(to: \.rates, on: self).store(in: &subscriptions)
        viewModel.$error.assign(to: \.error, on: self).store(in: &subscriptions)
    }
    
    private func configureNavigationBar() {
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = L10n.rates
        navigationItem.largeTitleDisplayMode = .always
        navigationItem.setRightBarButton(UIBarButtonItem(customView: activityIndicatorView), animated: true)
    }
    

    
    
    private func rateCellProvider(tableView:UITableView,indexPath:IndexPath,rate:RateViewModel) -> UITableViewCell? {
        RateTableViewCell.create(tableView: tableView, rate: rate)
    }
    
    private func updateRates(rates:[RateViewModel]) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, RateViewModel>()
        snapshot.appendSections([.main])
        snapshot.appendItems(rates, toSection: .main)
        tableViewDataSource.apply(snapshot, animatingDifferences: true,completion: self.tableView.reloadData)
    }
  


   
}
