//
//  CurrenciesViewModel.swift
//  BDSwiss Currency Rates
//
//  Created by Christos on 10/5/20.
//

import Foundation
import Combine
import UIKit.UIScene



class RatesViewModel:ObservableObject {
    
    @Inject private var properties:PropertiesManager
    @Inject private(set) var ratesRepository:RatesRepository
    @Inject private var notificationCenter:NotificationCenter
    
    private var timerSubscription:AnyCancellable?
    private var subscriptions = Set<AnyCancellable>()

    @Published var isLoadingPublisher:Bool = false
    @Published var rates:[RateViewModel] = []
    @Published var error:Error?

    
    init() {
        notificationCenter
            .publisher(for: UIScene.didEnterBackgroundNotification)
            .sink(receiveValue: {_ in self.timerSubscription?.cancel()})
            .store(in: &subscriptions)
        
        notificationCenter
            .publisher(for: UIScene.willEnterForegroundNotification)
            .sink(receiveValue: {_ in
                self.startGettingRates()
            })
            .store(in: &subscriptions)
    }
    
    deinit {
        subscriptions.cancelAll()
    }
    
    func startGettingRates()    {
        error = nil
        timerSubscription?.cancel()
        getRates()
        timerSubscription = Timer.publish(every: properties.refreshInterval, on: .main, in: .default).autoconnect().sink { _ in self.getRates()}
    }
    
    @objc func getRates() {
        do {
            self.isLoadingPublisher = true
            try self.ratesRepository.getRates()
                .compactMap(self.transformRates)
                .sink(receiveCompletion: self.onGetRatesCompletion, receiveValue: self.onRatesReceived)
                .store(in: &self.subscriptions)
        }
        catch {
            onError(error: error)
        }
    }
    
    private func onRatesReceived(rates:[RateViewModel]) {
        self.rates = rates
    }
    
    private func onGetRatesCompletion(completion:Subscribers.Completion<Error>) {
        switch completion {
        case .failure(let error):
            onError(error: error)
        case .finished:
            self.isLoadingPublisher = false
        }
    }
    
    private func onError(error:Error) {
        timerSubscription?.cancel()
        self.isLoadingPublisher = false
        self.error = error
    }
    
    private func transformRates(ratesObject:RatesObject) -> [RateViewModel] {
        return ratesObject.rates.map(self.transformRate)
    }
    
    private func transformRate(rate:Rate) -> RateViewModel {
        let previousPrice = rates.first(where: {$0.symbol == rate.symbol})?.price
        return RateViewModel(rate: rate,previousPrice: previousPrice)

    }
    
    
}

