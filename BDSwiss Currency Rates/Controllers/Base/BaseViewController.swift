//
//  BaseViewController.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 05/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit
import Combine

class BaseViewController:UIViewController {
    
    
    @Inject
    private var notificationCenter:NotificationCenter
    
    var subscriptions = Set<AnyCancellable>()
    
    override var navigationController: BaseNavigationController? {
        return super.navigationController as? BaseNavigationController
    }
    
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureObservers()
        addKeyboardObservers()
    }
    
    
    
    func configureObservers() {
        
    }
    
    deinit {
        removeKeyboardObservers()
        subscriptions.cancelAll()
    }
    
    private var errorAlertController:UIAlertController?
    
    func showError(error:Error, onDisappeared:@escaping ()->Void) {
        let title = L10n.error
        let okAction = UIAlertAction(title: L10n.ok, style: .default) { (_) in onDisappeared() }
        errorAlertController = UIAlertController(title: title, message: error.localizedDescription, preferredStyle: .alert)
        errorAlertController!.addAction(okAction)
        present(errorAlertController!, animated: true, completion: nil)
    }
    
    func hideError() {
        errorAlertController = nil
        errorAlertController?.dismiss(animated: true, completion: nil)
    }

    
    private func addKeyboardObservers() {
        notificationCenter.addObserver(self, selector: #selector(keyboardWillAppear(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func removeKeyboardObservers() {
        notificationCenter.addObserver(self, selector: #selector(keyboardWillAppear(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillAppear(notification: Notification) {
        let tabBarHeight = tabBarController?.tabBar.frame.height ?? 0
        onKeyboardStateChange(willOpen: true, height: notification.keyboardHeight - tabBarHeight, duration: notification.keyboardAnimation)
    }
    
    @objc private func keyboardWillHide(notification: Notification) {
        onKeyboardStateChange(willOpen: false, height: 0,duration: notification.keyboardAnimation)
    }
    
    
    open func onKeyboardStateChange(willOpen:Bool,height:CGFloat,duration:TimeInterval) {
        
    }
}
