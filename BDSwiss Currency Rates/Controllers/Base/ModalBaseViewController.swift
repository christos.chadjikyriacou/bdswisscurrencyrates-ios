//
//  ModalViewController.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 11/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit

class ModalBaseViewController:BaseViewController {
    private let onDismissed:(()->Void)? 
    private lazy var doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
    
    var showDoneItem:Bool = false {
        willSet {
            navigationItem.setLeftBarButton(newValue ? doneItem : nil, animated: true)
        }
    }
    
    @objc func done() {
        dismiss(animated: true, completion: nil)
    }
    
    init(onDismissed:(()->Void)? = nil) {
        self.onDismissed = onDismissed
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag) {
            self.onDismissed?()
            completion?()
        }
    }
    
    
}
