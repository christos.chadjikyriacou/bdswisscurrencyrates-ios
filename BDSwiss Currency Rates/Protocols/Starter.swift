//
//  Starter.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 07/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation

@objc public protocol Starter {
    @objc optional func start()
    static func start()
}
