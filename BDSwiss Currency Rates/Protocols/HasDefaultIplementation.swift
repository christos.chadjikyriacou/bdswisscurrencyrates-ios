//
//  HasDefaultIplementation.swift
//  BDSwiss Currency
//
//  Created by Christos Chadjikyriacou on 22/03/2020.
//  Copyright © 2020 NetInfo plc. All rights reserved.
//

import Foundation

public protocol HasDefaultIplementation {
    associatedtype T
    
    static var shared:T {get set}

}


extension HasDefaultIplementation {

    @discardableResult
    static func setNewDefaultImplementation(newImplementation:T) -> T {
        self.shared = newImplementation
        return newImplementation
    }
}
