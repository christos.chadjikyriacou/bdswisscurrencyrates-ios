//
//  Constants.swift
//  BDSwiss Currency Rates
//
//  Created by Christos on 10/6/20.
//

import Foundation


protocol PropertiesManager {
    var refreshInterval:TimeInterval { get }
    var priceFloatingPointPrecision:Int { get }
    var priceChangeAnimationDuration:TimeInterval { get }
}

class PropertiesManagerImpl:PropertiesManager {
    
    let refreshInterval:TimeInterval = 10
    let priceFloatingPointPrecision = 4
    let priceChangeAnimationDuration:TimeInterval = 0.4
    
    
}
