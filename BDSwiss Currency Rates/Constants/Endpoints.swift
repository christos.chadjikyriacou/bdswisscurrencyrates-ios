//
//  Endpoints.swift
//  BDSwiss Currency Rates
//
//  Created by Christos on 10/5/20.
//

import Foundation

class Endpoints {
    
    static let base = "https://mt4-api.bdswiss-staging.com"
    static let rates = base + "/rates"
}
