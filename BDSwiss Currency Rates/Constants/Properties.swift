//
//  Constants.swift
//  BDSwiss Currency Rates
//
//  Created by Christos on 10/6/20.
//

import Foundation


protocol PropertiesManager {
    var refreshInterval:TimeInterval { get }
    var priceFloatingPointPrecision:Int { get }
    var priceChangeAnimationDuration:TimeInterval { get }
}

class PropertiesImpl:PropertiesManager {
    
    let refreshInterval:TimeInterval = 1
    let priceFloatingPointPrecision = 4
    let priceChangeAnimationDuration:TimeInterval = 0.4
    
    
}
