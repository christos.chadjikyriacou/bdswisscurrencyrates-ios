//
//  File.swift
//  BDSwiss Currency Rates
//
//  Created by Christos on 10/5/20.
//

import Foundation


struct RatesObject: Codable {
    let rates: [Rate]
}


struct Rate: Codable {
    let symbol: String
    let price: Double
}

extension Rate:Hashable {
    
    static func == (lhs: Rate, rhs: Rate) -> Bool {
        return lhs.symbol == rhs.symbol
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(symbol)
    }
    
}
