//
//  File.swift
//  
//
//  Created by Christos Chadjikyriacou on 26/02/2020.
//

import Foundation



@propertyWrapper
public struct UserDefaultStorage<T> {
    let key: String
    let defaultValue:T
    
    
    public var wrappedValue: T {
        get {
            return (UserDefaults.standard.object(forKey: key) as? T) ?? defaultValue
        }
        set {
            UserDefaults.standard.set(newValue, forKey: key)
        }
    }
}

public extension UserDefaultStorage where T:ExpressibleByNilLiteral {
    init(key: String) {
        self.init(key: key, defaultValue: nil)
    }
}

public extension UserDefaultStorage where T == Bool {
    init(key: String) {
        self.init(key: key, defaultValue: false)
    }
}


