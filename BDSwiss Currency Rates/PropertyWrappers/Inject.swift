//
//  Inject.swift
//  BDSwiss Currency
//
//  Created by Christos Chadjikyriacou on 21/03/2020.
//  Copyright © 2020 NetInfo plc. All rights reserved.
//

import Foundation

@propertyWrapper
public struct Inject<Service> {

    public var wrappedValue:Service
    
    public init() {
        self.init(name: nil)
    }
    
    public init(name: String? = nil,from container:DependeciesContainer? = nil) {
        let defaultContainer = DependeciesContainer.shared
        
        guard let value = (container ?? defaultContainer)
            .resolve(serviceType: Service.self, name: name)  else {
            fatalError("Could not resolve non-optional \(Service.self)")
        }
        
        wrappedValue = value
    }
    
    
}

extension Inject where Service == ExpressibleByNilLiteral {
    public init(name: String? = nil,from container:DependeciesContainer? = nil) {
         let defaultContainer = DependeciesContainer.shared
        wrappedValue = (container ?? defaultContainer).resolve(serviceType: Service.self, name: name)
    }
}


