//
//  DependencyInjectionTests.swift
//  BDSwiss Currency RatesTests
//
//  Created by Christos on 10/8/20.
//

import Foundation
import XCTest
import Combine
@testable import BDSwiss_Currency_Rates

class NetworkingTests:XCTestCase {

    private var subscriptions = Set<AnyCancellable>()
    func testNetworkCall() {
        
        let mainThreadExpectation = expectation(description: "Response is in main thread")
        let correctResponseExpectation = expectation(description: "Received correct response")
        
        do {
            let endpoint =  Endpoints.rates
            let request =  try NetworkManager.Request(urlString: endpoint, httpMethod: .get)
            try NetworkManager.shared.run(request).sink(receiveCompletion: { (completion) in
                switch completion {
                case .failure(let error):
             
                    XCTFail(error.localizedDescription)
                case .finished:
                    break
                }
            }, receiveValue: { (ratesObject:RatesObject) in
                if Thread.isMainThread {
                    mainThreadExpectation.fulfill()
                }
                
                if !ratesObject.rates.isEmpty {
                    correctResponseExpectation.fulfill()
                }
            }).store(in: &subscriptions)
        }
        catch {
            XCTFail(error.localizedDescription)
        }
        
        wait(for: [mainThreadExpectation,correctResponseExpectation], timeout: 30)

    }
    
}
