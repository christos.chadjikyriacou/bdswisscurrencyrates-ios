//
//  Extensions Tests.swift
//  BDSwiss Currency RatesTests
//
//  Created by Christos on 10/6/20.
//

import XCTest
@testable import BDSwiss_Currency_Rates

class ExtensionsTests:XCTestCase {
    
    
    func testStringToURL() {
        let correctURL1 = "http://localhost:4000/api/Test"
        let correctURL2 = "https://localhost:4000/api/Test"
        let correctURL3 = "/api/Test"
        let correctURL4 = "htt://localhost:4000/api/Test"
        let wrongURL1 = ""
        let wrongURL2 = "bad url"
        
        
        XCTAssertNil(wrongURL1.toURL)
        XCTAssertNil(wrongURL2.toURL)
        
        XCTAssertNotNil(correctURL1.toURL)
        XCTAssertNotNil(correctURL2.toURL)
        XCTAssertNotNil(correctURL3.toURL)
        XCTAssertNotNil(correctURL4.toURL)
        
    }
    
    func testSafeArray() {
        let array = ["test1","test2","test3"]
        
        XCTAssertNil(array[safe: -1])
        XCTAssertNil(array[safe: 3])
        XCTAssertEqual(array[safe:0], array.first)
    }
    
    
    func testDoubleToString() {
        
        let testDouble = 1.22324234234
        
        XCTAssertEqual(testDouble.toString(with: -1),"")
        XCTAssertEqual(testDouble.toString(with: 0),"1")
        XCTAssertEqual(testDouble.toString(with: 2),"1.22")
        XCTAssertEqual(testDouble.toString(with: 4),"1.2232")
        XCTAssertEqual(testDouble.toString(with: 6),"1.223242")
    }
    
    
}
