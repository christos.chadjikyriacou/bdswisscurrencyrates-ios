//
//  TestResources.swift
//  BDSwiss Currency RatesTests
//
//  Created by Christos on 10/7/20.
//

import Foundation
@testable import BDSwiss_Currency_Rates

class TestResources {
    
    
    static let ratesTestWrongJSON = """
    {
       "rates":[
          {
             "symbol":"EURUSD",
             "price":"notADouble"
          },
          {
             "symbol":"GBPUSD",
             "price":1.3152476318962112
          },
          {
             "symbol":"EURGBP",
             "price":0.9089926446114674
          },
          {
             "symbol":"GBPCHF",
             "price":1.1997457098823407
          }
       ]
    }
    """
    
    static let ratesTestCorrectJSON = """
    {
       "rates":[
          {
             "symbol":"EURUSD",
             "price":1.099207977518292
          },
          {
             "symbol":"GBPUSD",
             "price":1.3283924351995655
          },
          {
             "symbol":"EURGBP",
             "price":0.8645656815161293
          },
          {
             "symbol":"GBPCHF",
             "price":1.1755414804422097
          }
       ]
    }
    """
    
    
    static var mockPreviousRates:RatesObject = {
        return RatesObject(rates: [
            Rate(symbol:"EURGBR",price:1.099207977518292),
            Rate(symbol:"GBPUSD",price:1.3283924351995655),
            Rate(symbol:"EURGBP",price:0.8645656815161293),
            Rate(symbol:"GBPCHF",price:1.1755414804422097)
        
        ])
    }()
    
    static var mockRates:RatesObject = {
        return RatesObject(rates: [
            Rate(symbol:"EURGBR",price:2.099207977518292),
            Rate(symbol:"GBPUSD",price:0.3283924351995655),
            Rate(symbol:"EURGBP",price:0.8645656815161293),
            Rate(symbol:"GBPCHF",price:1.1755414804422097)
        
        ])
    }()

    
}
