//
//  ModelTest.swift
//  BDSwiss Currency RatesTests
//
//  Created by Christos on 10/6/20.
//

import XCTest
@testable import BDSwiss_Currency_Rates

class RatesTests:XCTestCase {
    
    
    func testRatesModelWithCorrectJSON() {
        self.measure {
            guard let json = TestResources.ratesTestCorrectJSON.data(using: .utf8) else {
                XCTFail("Unable to convert json to data")
                return
            }
            
            do {
                
                let decoder = JSONDecoder()
                let ratesObject = try decoder.decode(RatesObject.self, from: json)
                
                
                XCTAssertEqual(ratesObject.rates.count, 4)
                XCTAssertEqual(ratesObject.rates.first!.symbol, "EURUSD")
                XCTAssertEqual(ratesObject.rates.first!.price, 1.099207977518292)
                XCTAssertEqual(ratesObject.rates.last!.symbol, "GBPCHF")
                XCTAssertEqual(ratesObject.rates.last!.price, 1.1755414804422097)
                
            }
            catch {
                XCTFail(error.localizedDescription)
            }
        }
        
    }
    
    func testRatesModelWithWrongJSON() {
        
        guard let json = TestResources.ratesTestWrongJSON.data(using: .utf8) else {
            XCTFail("Unable to convert json to data")
            return
        }
        
        let decoder = JSONDecoder()
        XCTAssertThrowsError(try decoder.decode(RatesObject.self, from: json))
        
        
    }
    
    func testRatesModelEquality() {
        let rate1 = Rate(symbol:"EURGBP",price:1.21331232)
        let rate2 = Rate(symbol:"EURGBP",price:0.98938949)
        let rate3 = Rate(symbol:"GBPEUR",price:2.12343434)
        
        XCTAssertEqual(rate1, rate2)
        XCTAssertNotEqual(rate2, rate3)
    }
    
    
    
}
