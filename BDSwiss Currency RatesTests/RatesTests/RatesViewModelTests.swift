//
//  RatesViewModelTest.swift
//  BDSwiss Currency RatesTests
//
//  Created by Christos on 10/7/20.
//

import XCTest
import Combine
@testable import BDSwiss_Currency_Rates

class RatesViewModelTests:XCTestCase {
    
    private var subscriptions = Set<AnyCancellable>()
    private let properties = MockPropertiesManager()
    private var rates = [RateViewModel]()
    private lazy var ratesViewModel:RatesViewModel = {
        let viewModel = RatesViewModel()
        viewModel.$rates.assign(to: \.rates, on: self).store(in: &subscriptions)
        return viewModel
    }()
    
    
    
    
    override func setUp() {
        let testContainer = DependeciesContainer()
        testContainer.register(serviceType: NotificationCenter.self, resolve: {NotificationCenter.default})
        testContainer.register(serviceType: PropertiesManager.self, resolve: {MockPropertiesManager()})
        testContainer.register(serviceType: RatesRepository.self, resolve: {MockRatesRepository()})
        DependeciesContainer.setNewDefaultImplementation(newImplementation: testContainer)
        
        //We are doing this so can have a previous price
        ratesViewModel.getRates()
        ratesViewModel.getRates()
 
    }
    
   
    func testGetRatesNotEmpty() {
        XCTAssertEqual(rates.count, TestResources.mockRates.rates.count)
    }
    
    
    
    func testRatesHasCorrectSymbol() {
        XCTAssertEqual(rates.first?.symbol, TestResources.mockRates.rates.first?.symbol)
    }
    
    
    func testRatesHasCorrectPrice() {
        XCTAssertEqual(rates.first?.price, TestResources.mockRates.rates.first?.price)
    }

    func testRatesHasCorrectPriceFloatingPointPrecision() {
        XCTAssertEqual(rates.first?.priceString.split(separator: ".").last?.count, properties.priceFloatingPointPrecision)
    }
    
    func  testRatesHasCorrectPriceChangeValue() {
        XCTAssertEqual(rates[safe:0]?.priceChange, .equealOrHigher)
        XCTAssertEqual(rates[safe:1]?.priceChange, .lower)
        XCTAssertEqual(rates[safe:2]?.priceChange, .equealOrHigher)
    }
    
    
    func testStartGettingRatesInterval() {
        let expectation = self.expectation(description: "First Tick")
        let expectation2 = self.expectation(description: "Second Tick")
        let expectation3 = self.expectation(description: "Third Tick")
        
        let ratesViewModel = RatesViewModel()
        var tickedTimes = 0
        let latestTimeStamp = Date()
        ratesViewModel.$rates.sink(receiveValue: { rateViewModels in
            guard !rateViewModels.isEmpty else { return }
            let passedTime =  round(Date().timeIntervalSince(latestTimeStamp))
            
            if tickedTimes == 0 && passedTime == 0 {
                expectation.fulfill()
            }
            
            if tickedTimes == 1 && passedTime == self.properties.refreshInterval{
                expectation2.fulfill()
            }
            
            if tickedTimes == 2 && passedTime == self.properties.refreshInterval * 2{
                expectation3.fulfill()
            }
            
            tickedTimes += 1
            
            
        }).store(in: &subscriptions)
        
        ratesViewModel.startGettingRates()
        wait(for: [expectation,expectation2,expectation3], timeout: properties.refreshInterval * 4)
        
        
    }
    
}
