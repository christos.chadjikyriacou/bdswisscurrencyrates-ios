//
//  MockRatesRepository.swift
//  BDSwiss Currency RatesTests
//
//  Created by Christos on 10/7/20.
//

import Foundation
import Combine
@testable import BDSwiss_Currency_Rates

class MockRatesRepository:RatesRepository {
    
    private(set) var hasAlreadyCalled = true
    
    func getRates() throws -> AnyPublisher<RatesObject, Error> {
        let mockData = hasAlreadyCalled ? TestResources.mockPreviousRates : TestResources.mockRates
        let publisher = CurrentValueSubject<RatesObject,Error>(mockData).eraseToAnyPublisher()
        hasAlreadyCalled.toggle()
        return publisher
        
    }
}
