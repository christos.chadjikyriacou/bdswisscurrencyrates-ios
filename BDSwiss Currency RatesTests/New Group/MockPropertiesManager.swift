//
//  MockPropertiesManager.swift
//  BDSwiss Currency RatesTests
//
//  Created by Christos on 10/7/20.
//

import Foundation
import Combine
@testable import BDSwiss_Currency_Rates

class MockPropertiesManager:PropertiesManager {
    let refreshInterval: TimeInterval = 1
    var priceFloatingPointPrecision: Int = 4
    var priceChangeAnimationDuration: TimeInterval = 0.4

}
